import React, {useState, useEffect} from 'react';
import messaging from '@react-native-firebase/messaging';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import FirstScreen from '../screens/FirstScreen';
import LoginPage from '../screens/LoginPage';

import ApplyLeave from '../screens/ApplyLeave';
import ListOfProjects from '../screens/ListOfProjects';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import PushNotification from "react-native-push-notification";
import Header from '../components/Header';
import {Provider, useDispatch}  from  'react-redux';
import store from '../redux/store'
import BudgetTrackers from '../screens/BudgetTrackers';
import BudgetFilters from '../screens/BudgetFilters';
import AddFixedExpenses from '../screens/AddFixedExpenses';
import AddPayments from '../screens/AddPayments';
import SplashScreen from '../screens/SplashScreen';
import UpdateFixedExpenses from '../screens/UpdateFixedExpenses';
import UpdateReceivedPayments from '../screens/UpdateReceivedPayments'
import ListOfLeaveRequest from  '../screens/ListOfLeaveRequest'
import ViewLeaveRequest from '../screens/ViewLeaveRequest'



console.reportErrorsAsExceptions = false;

const Navigator = () => {
  const Stack = createNativeStackNavigator();
  const dispatch = useDispatch
  const [login,setLogin] = useState(false);

  // const [initialRoute, setinitialRoute] = useState('FirstScreen');




  // Must be outside of any component LifeCycle (such as `componentDidMount`).
PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function (token) {
    console.log("TOKEN:", token);
  },

  // (required) Called when a remote is received or opened, or local notification is opened
  onNotification: function (notification) {
    console.log("NOTIFICATION:", notification);

    // process the notification

    // (required) Called when a remote is received or opened, or local notification is opened
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  // (optional) Called when Registered Action is pressed and invokeApp is false, if true onNotification will be called (Android)
  onAction: function (notification) {
    console.log("ACTION:", notification.action);
    console.log("NOTIFICATION:", notification);

    // process the action
  },

  // (optional) Called when the user fails to register for remote notifications. Typically occurs when APNS is having issues, or the device is a simulator. (iOS)
  onRegistrationError: function(err) {
    console.error(err.message, err);
  },

});



  const mail = async () => {
    await messaging().registerDeviceForRemoteMessages();
    const token = await messaging().getToken();
    console.log('token', token);
  }


  useEffect(() => {
    // getData();
    mail();
   
  }, []);



  return (
    <Provider  store={store}>
    <NavigationContainer independent={true}  >
      <Stack.Navigator
        screenOptions={{headerShown: false}}
        initialRouteName='FirstScreen'>
        <Stack.Screen name = "LoginPage" component = {LoginPage}/>
        <Stack.Screen name="FirstScreen" component={FirstScreen} />
        <Stack.Screen name = "ApplyLeave" component = {ApplyLeave}/>
        <Stack.Screen name = "ListOfLeaveRequest" component = {ListOfLeaveRequest}/>
        <Stack.Screen name="ViewLeaveRequest" component={ViewLeaveRequest}/>
        <Stack.Screen name = "ListOfProjects" component ={ListOfProjects}/>
        <Stack.Screen name = "BudgetTrackers" component ={BudgetTrackers}/>
        <Stack.Screen name = "BudgetFilters" component ={BudgetFilters}/>
        <Stack.Screen name = "AddFixedExpenses" component ={AddFixedExpenses}/>
        <Stack.Screen name = "AddPayments" component ={AddPayments}/>
        <Stack.Screen name = "SplashScreen"  component = {SplashScreen}/>
        <Stack.Screen name="UpdateFixedExpenses"  component={UpdateFixedExpenses}/>
        <Stack.Screen name = "UpdateReceivedPayments" component={UpdateReceivedPayments} /> 
       
      </Stack.Navigator>
    </NavigationContainer>
    </Provider>
  );
};

const styles = StyleSheet.create({});

export default Navigator;
