import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {useState, useEffect} from 'react';
import Navigator from './Navigator';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Provider} from 'react-redux';
import store from '../redux/store';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {setUserID} from '../redux/slice/User';
import LoginPage from '../screens/LoginPage';
import SplashScreen from '../screens/SplashScreen';

const RootNavigation = () => {
  const navigation = useNavigation();
  const [login, setLogin] = useState(false);
  const dispatch = useDispatch();
  const userID = useSelector(state => state.user.userID);
  const [splashScreen1, setSplashScreen] = useState(false);

  useEffect(() => {
    screen();
    getData();

  }, []);
  const screen = () => {
    setTimeout(() => {
      setSplashScreen(true);
    }, 3000);
    return () => {
      clearTimeout(screen);
    };
  };

  let getData = async () => {
    try {
      const value = await AsyncStorage.getItem('@store');
      console.log('getstore value', value);
      dispatch(setUserID(value));
    } catch (e) {
      alert('no data from storage');
    }
  };
  return (
    <Provider store={store}>
      <NavigationContainer independent={true}>
        {splashScreen1 ? (
          userID ? (
            <Navigator />
          ) : (
            <LoginPage />
          )
        ) : (
          <SplashScreen />
        )}
      </NavigationContainer>
    </Provider>
  );
};

export default RootNavigation;

const styles = StyleSheet.create({});