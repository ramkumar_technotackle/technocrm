export const colours ={
    Green :  '#01b298',
    White :  '#FFFFFF',
    Black :  '#000000',
    Red   :  '#FF0000',
    Blue :  '#0000ff',
    MetalicGold : "#d4af37",
}

export const baseUrl = 'https://incentive.bacet.org/v1/';
export const SALT = 'TTCRM@2020' ;
export const LOG_IN = baseUrl+'developer/login';
export const APPLY_LEAVE =  baseUrl + 'leaverequest/apply';
export const LIST_OF_LEAVE_REQUESTS = baseUrl + 'leaverequest'
export const LIST_OF_PROJECTS = baseUrl+'budgettracker/getProjects';
export const GET_PROJECT_USERS = baseUrl + 'budgettracker/getProjectUsers'
export const BUDGET_TRACKER = baseUrl + 'budgettracker'
export const ADD_FIXED_EXPENSES = baseUrl + 'fixedexpenses/add';
export const ADD_RECEIVED_PAYMENTS = baseUrl +'paymentsreceived/add';
export const UPDATE_FIXED_EXPENSES = baseUrl + 'fixedexpenses/update' ;
export const DELETE_FIXED_EXPENSES = baseUrl + 'fixedexpenses/delete';
export const UPDATE_RECEIVED_PAYMENTS = baseUrl +'paymentsreceived/update';
export const DELETE_RECEIVED_PAYMENTS = baseUrl+'paymentsreceived/delete';


// export const baseUrl = 'https://demo.technotackle.com/crm/v1/';
// export const baseUrl = 'https://dev.bacet.org/crm/v1/';