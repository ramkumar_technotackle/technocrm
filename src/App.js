
import React from 'react';
import { LogBox } from 'react-native';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import { useState, useEffect } from 'react';
import Navigator from './navigation/Navigator'
import AsyncStorage from '@react-native-async-storage/async-storage';
import SplashScreen from './screens/SplashScreen';
import LoginPage from './screens/LoginPage';
import { Provider } from 'react-redux';
import store from '../src/redux/store';
import {NavigationContainer, useNavigation} from '@react-navigation/native';
import { navigationRef } from './RootNavigation';
// import { useDispatch } from 'react-redux';
import { setUser, setUserID } from './redux/slice/User';
import { useSelector } from 'react-redux';
import RootNavigation from './navigation/RootNavigation';



const App = () => { 
  LogBox.ignoreAllLogs();

  return (
 <Provider   store={store}>
<NavigationContainer independent={true}  >



 <RootNavigation />  

</NavigationContainer>
</Provider> 



  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
