import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Button,
  Image,
  TouchableOpacity,
  TextInput,
  Alert,
  ActivityIndicator,
} from 'react-native';
import React, {Fragment, useRef, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {Formik} from 'formik';
import * as yup from 'yup';
import md5 from 'md5';
import axios from 'axios';
import {setUser, setUserID} from '../redux/slice/User';
import {useDispatch} from 'react-redux';
import SplashScreen from './SplashScreen';
import {useEffect} from 'react';
import {colours, LOG_IN} from '../constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import User from 'react-native-vector-icons/AntDesign';
import Eyeoff from 'react-native-vector-icons/Feather';
import Eyeon from 'react-native-vector-icons/Feather';
import Unlock from 'react-native-vector-icons/Feather';
// RNBootSplash.hide(); // immediate
// RNBootSplash.hide({ fade: false }); // fade

const {width, height} = Dimensions.get('window');

const LoginPage = () => {
  const navigation = useNavigation();
  const pattern2 = /^[a-z]+[@][a-z]*$/;
  const formRef = useRef();
  const [details, setDetails] = useState();
  const [loading, setLoading] = useState();
  const [name, setName] = useState();
  const [eye, setEye] = useState(true);
  console.log('details', name);

  const SALT = 'TTCRM@2020';
  const dispatch = useDispatch();

  const handleSubmit = () => {
    if (formRef.current) formRef.current.handleSubmit();
  };

  const validationSchema = yup.object().shape({
    username: yup.string().required('Username required'),

    password: yup.string().required('Password required'),
  });

  const onSubmit = values => {
    setLoading(true);
    setName(values.username);
    console.log('values', values);
    const auth_token = md5(SALT + values.username + values.password);
    const receiveEnd = LOG_IN;
    const data1 = {
      username: values.username,
      password: values.password,
      auth_token: auth_token,
    };

    axios
      .post(receiveEnd, data1)
      .then(Response => {
        if (Response.data.success) {
          setLoading(false);
          dispatch(setUser(Response.data.parameters));
          storeData(Response.data.parameters.id);
          setDetails(Response.data);
          console.log('response data', Response.data);
          navigation.navigate('FirstScreen');
        } else {
          setLoading(false);
          alert(Response.data.message);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  let storeData = async storeddata => {
    try {
      dispatch(setUserID(storeddata));
      await AsyncStorage.setItem('@store', storeddata);
      console.log('store value is', storeddata);
    } catch (e) {
      alert('failed to save data');
    }
  };

  // const handleEyeSubmit=()=>{
  //   setEye(false);

  // }

  return (
    <ScrollView style={styles.head}>
      <View>
        <View style={styles.photo}>
          <View>
            <Image
              style={styles.logoImage}
              source={require('../assets/Images/TechnoLogo.png')}
            />
          </View>
        </View>
        <Text style={styles.HeadText}>TECHNO TACKLE</Text>
        <Text style={styles.HeadText2}>Software Solutions,Coimbatore</Text>

        <Text style={styles.HeadText3}>
          Customer Relationship Management (CRM)
        </Text>

        <Text style={styles.HeadText4}>Use your Techno Tackle account</Text>
      </View>
      <Formik
        innerRef={formRef}
        initialValues={{username: '', password: ''}}
        onSubmit={onSubmit}
        validationSchema={validationSchema}>
        {formikProps => (
          <Fragment>
            <View style={styles.head3}>
              <View style={styles.labelBox}>
                <Text style={styles.labelText}>Username</Text>
                <Text style={styles.star}>*</Text>
              </View>
              <View style={styles.inputBox}>
                <User name="user"color={colours.Green} size={22} />
                <TextInput
                  name={'username'}
                  onChangeText={formikProps.handleChange('username')}
                  onBlur={formikProps.handleBlur('username')}
                  value={formikProps.values.username}
                  placeholder="Username"
                  placeholderTextColor="#01b298"
                  style={styles.username}></TextInput>
              </View>

              {formikProps.errors.username && formikProps.touched.username ? (
                <Text style={styles.errors}>{formikProps.errors.username}</Text>
              ) : null}
              <View style={{...styles.labelBox, marginTop: 30}}>
                <Text style={styles.labelText}>Password</Text>
                <Text style={styles.star}>*</Text>
              </View>

              <View style={styles.inputBox}>
              <Unlock name="unlock" color={colours.Green} size={22} />
                <TextInput
                  secureTextEntry={eye}
                  onChangeText={formikProps.handleChange('password')}
                  onBlur={formikProps.handleBlur('password')}
                  value={formikProps.values.password}
                  placeholder="Password"
                  placeholderTextColor="#01b298"
                  style={styles.username}>
                </TextInput>
                <TouchableOpacity
                onPress={()=>{setEye(!eye)}}
                  style={styles.maskPassword}>
                  {eye ? (
                    <Eyeoff name="eye-off" color={colours.Green} size={22} />
                  ) : (
                    <Eyeon name="eye" color={colours.Green} size={22} />
                  )}
                </TouchableOpacity>
              </View>

              {formikProps.errors.password && formikProps.touched.password ? (
                <Text style={styles.errors}>{formikProps.errors.password}</Text>
              ) : null}

              {loading ? (
                <View>
                  <ActivityIndicator size="large" color="#00ff00" />
                </View>
              ) : (
                <TouchableOpacity style={styles.button1} onPress={handleSubmit}>
                  <Text style={styles.loginButtonText}>Login</Text>
                </TouchableOpacity>
              )}
            </View>
          </Fragment>
        )}
      </Formik>
    </ScrollView>
  );
};

export default LoginPage;

const styles = StyleSheet.create({
  head: {
    height: height,
    width: width,
    backgroundColor: '#FFFFFF',
  },

  photo: {
    height: height * 0.15,
    width: width,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
  },
  logoImage: {
    height: 140,
    width: 165,
    marginTop: 20,
  },
  head3: {
    width: width * 0.91,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colours.White,
    marginTop: 50,
    
  },
  HeadText: {
    color: '#01b298',
    marginTop: 20,
    width: width,
    fontSize: 25,
    textAlign: 'center',
    fontWeight: '500',
  },
  HeadText2: {
    color: '#01b298',
    fontSize: 14,
    textAlign: 'center',
  },
  HeadText3: {
    color: '#01b298',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 30,
  },
  HeadText4: {
    color: '#01b298',
    fontSize: 14,
    textAlign: 'center',
    marginTop: 30,
    fontWeight: '700',
  },


  errors: {
    fontSize: 13,
    color: 'red',
    alignSelf: 'flex-start',
  },
  button1: {
    width: width * 0.8,
    alignItems: 'center',
    justifyContent:'center',
    backgroundColor: colours.Green,
    marginTop: 50,
    alignSelf: 'center',
    borderRadius: 8,
    height:height*0.07
  },
  logo: {
    height: height * 0.2,
    width: width * 0.5,
    marginBottom: 30,
  },
  labelText: {
    alignSelf: 'flex-start',
    color: colours.Green,
  },
  labelBox: {
    alignSelf: 'flex-start',
    marginBottom: 10,
    flexDirection: 'row',
  },
  loginButtonText: {
    color: colours.White,
    fontSize: 25,
   marginBottom:4,
   textAlign:'center',
  },
  star: {
    color: colours.Red,
  },
  inputBox: {
    borderWidth: 1,
    borderColor: '#01b298',
    width: width * 0.904,
    backgroundColor: '#FFFFFF',
    paddingLeft: 5,
    paddingRight: 5,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },  
  
  username: {
    borderColor: colours.Green,
    backgroundColor: colours.White,
    borderRadius: 8,
    flex:1,
    color: colours.Black
  },
  maskPassword:{
    justifyContent: 'center',
    alignItems: 'center',
    
  }
});
