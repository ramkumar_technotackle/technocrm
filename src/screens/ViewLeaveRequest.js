 import { useRoute } from '@react-navigation/native';
import React from 'react';
 import {View,Text,StyleSheet,SafeAreaView,Dimensions} from 'react-native';
import { useSelector } from 'react-redux';
 import Header from '../components/Header';
import { colours } from '../constants';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
 const ViewLeaveRequest = ()=>{
const leaveRequests = useSelector(state=>state.user.leaveRequests);
console.log('view leave', leaveRequests)

const leaveDetails = useRoute().params.leaveDetail;
console.log('leave details', leaveDetails);

    return(
<SafeAreaView>
    <Header name="View Leave" />
    <View  style={styles.main}>
        <View  style = {styles.dataCard}><View style={{flex:1,paddingLeft:3,paddingRight:3}}>
                              <View style={styles.dataBoxHeader}>
                            <Text style={{ ...styles.dataCardText, fontWeight: '500' }}>
                            {leaveDetails.developer.developer_name}  
                            </Text>
                        </View>

                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Leave Type
                            </Text>
                            <Text style={styles.dataTextRight}>
                            {leaveDetails.leave_type}
                            </Text>
                        </View>


        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                From Time
                            </Text>
                            {leaveDetails.from_time ?
                            <Text style={styles.dataTextRight}>
                            {leaveDetails.from_time} 
                            </Text> :
                            <Text style={styles.dataTextRight}>
                            --
                            </Text> }
                        </View>

                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                To Time
                            </Text>
                            {leaveDetails.to_time ?
                            <Text style={styles.dataTextRight}>
                            {leaveDetails.to_time} 
                            </Text> :
                            <Text style={styles.dataTextRight}>
                            --
                            </Text> }
                        </View>

                        
                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Date Of Leave
                            </Text>
                            <Text style={styles.dataTextRight}>
                            {leaveDetails.date_of_leave}
                            </Text>
                        </View>

                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Comments
                            </Text>
                            <Text style={styles.dataTextRight}>
                            {leaveDetails.dev_comments}
                            </Text>
                        </View>
                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Status
                            </Text>
                            <Text style={styles.dataTextRight}>
                            {leaveDetails.status}
                            </Text>
                        </View>

                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Created at
                            </Text>
                            <Text style={styles.dataTextRight}>
                            {leaveDetails.created_at}
                            </Text>
                        </View>

                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                status Updated at
                            </Text>
                            {leaveDetails.upadated_at ?
                            (<Text style={styles.dataTextRight}>
                            {leaveDetails.upadated_at}
                            </Text>) :
                             (<Text style={styles.dataTextRight}>
                             Not Set
                             </Text>) }
                        </View>
                        </View>
        </View>
    </View>
    </SafeAreaView>

    )
 }
 export default ViewLeaveRequest

 const styles = StyleSheet.create({
    main:{
        height :height*1,
        width:width*1,
        alignItems:'center',
    },
    dataCard:{
        height: height * 0.6,
        width: width * 0.9,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        elevation: 4,
        marginVertical:10,
        backgroundColor: '#ffffff',
        alignSelf: 'center',
        paddingRight: 3,
        paddingLeft: 3, 

    },
    dataBox: {
        marginVertical: 10,
        padding: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderWidth: 0.5,
        borderRadius:5,
        backgroundColor: '#F9F9F9',
    },
    dataCardText: {
        color: colours.Black,
        fontSize: 15,
    },
    dataTextRight: {
        fontSize: 15,
    },
    dataBoxHeader:{
        marginVertical: 5,
        padding: 5,
        justifyContent: 'center', 
        alignItems:'center',
        borderBottomWidth: 1,
         backgroundColor: '#f2e58c', 
        borderWidth: 1,
         borderColor: colours.Green,
    }

 })