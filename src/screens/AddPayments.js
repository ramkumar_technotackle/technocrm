import { Dimensions, StyleSheet, Text, View,TouchableOpacity,TextInput,ScrollView } from 'react-native'
import React from 'react'
import Header from '../components/Header';
import { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';
import axios from 'axios';
import md5 from 'md5';
import { SALT,ADD_RECEIVED_PAYMENTS,colours } from '../constants';



const height =  Dimensions.get('window').height;
const width =  Dimensions.get('window').width;
const AddPayments = () => {
    const user = useSelector(state => state.user.parameters);
    const userID =useSelector(state=>state.user.userID);
    console.log('userId is',userID);
    const projects = useSelector(state => state.user.projects);
    const navigation = useNavigation();
    const [remarks,setRemarks] = useState('');
    const [amount,setAmount]   = useState('');
    const [addPayments,setAddPayments]  = useState();
    const [remarksError, setRemarksError] = useState(false);
  const [amountError, setAmountError] = useState(false);
  const [amountError1, setAmountError1] = useState(false);
  const [amountText, setAmountText] = useState();
    const patternAmount = /^[0-9]{1,6}$/;
    const decimalAmount = /^[0-9]$/;
    console.log('remarks is ', remarks)




    const handleSubmit =()=>{

      if(remarks ==='') {
        setRemarksError(true);
      } else if (amount === '') {
        setAmountError(false);
        setAmountError1(true);
        setAmountText('Please add amount*');
      }
      else if (patternAmount.test(amount)) {
        projects.map(item => {
            console.log('projectID', item.name, item.id_string);
            const projectID = item.id_string;
            const receiveEnd = ADD_RECEIVED_PAYMENTS;
            const userID1 = userID;
            const auth_token = md5(SALT + userID + projectID+remarks);
      
            const data1 = {
              developer_id: userID1,
              project_id: projectID,
              remarks:remarks,
              amount:amount,
              auth_token: auth_token,
            };
            console.log(' fixed data1 is', data1);
            axios
              .post(receiveEnd, data1)
              .then(Response => {
                if (Response.data.success) {
                  setAddPayments(Response.data.parameters);
                  console.log('added Payments is',Response.data.parameters)
                  navigation.navigate('BudgetTrackers');
                } else {
                  alert('Please fill mandatory fields', Response.data.message);
                }
              })
              .catch(err => {
                console.log(err);
              });
          });
        } else {
          if(decimalAmount.test(amount)){
            setAmount('');
          alert('maximum limit below 10lacs and value should be 0-9');
          // alert('Decimal value not allowed')
          }else{
            setAmount('');
          alert('Decimal value not allowed');
          }
          // setAmount('');
          // alert('maximum limit below 10lacs and value should be 0-9');
        }
        };
    




  return (
    <ScrollView  >
        <Header name="Add Payments" />
        <View style={styles.main}>
    <View style={styles.formBox}>
        <View style = {styles.inputBox}>
          <View style={styles.label1} ><Text  style={styles.labelText}>Remarks</Text>
          <Text  style={styles.labelStar}>*</Text>
         </View>
        <TextInput
            placeholder="Remarks"
            placeholderTextColor={colours.Green}
            style={styles.input1}
            onChangeText={(value)=>{ setRemarks(value)
            setRemarksError(false)}}
            ></TextInput>
            {remarksError ? (
              <View style={styles.label1}>
                <Text style={{...styles.labelText, color: colours.Red}}>
                  Please enter remarks*
                </Text>
              </View>
            ) : null}


          <View style={styles.label1} ><Text  style={styles.labelText}>Amount</Text>
          <Text  style={styles.labelStar}>*</Text>
         </View>
<TextInput
            value={amount}
            placeholder="Amount"
            placeholderTextColor={colours.Green}
            style={styles.input1}
            keyboardType="number-pad"
            onChangeText={value => {
              setAmount(value);
              if(value.length>6){
                setAmountError(true);}
                else{
                  setAmountError(false);
                }
                setAmountError1(false);
            }}></TextInput>
            {amountError ? (
              <View style={styles.label1}>
                <Text style={{...styles.labelText, color: colours.Red}}>
                  maximum limit is below 10lac
                </Text>
              </View>
            ) : null}
            {amountError1 ? (
              <View style={styles.label1}>
                <Text style={{...styles.labelText, color: colours.Red}}>
                  {amountText}
                </Text>
              </View>
            ) : null}
        </View>

        
            <TouchableOpacity onPress={()=>{handleSubmit()}} style={styles.button1} >
              <Text
                style={styles.buttonText}
                >
                Save
              </Text>
            </TouchableOpacity>
          
      
    </View>
    </View>
    </ScrollView>
  )
}

export default AddPayments

const styles = StyleSheet.create({ 
     main:{
      height :height,
      alignItems: 'center',
      justifyContent:'center',
      backgroundColor : colours.White,
     
  },
  formBox:{
      height: height*0.4,
      width : width,
      backgroundColor : colours.White,
     
  },
  inputBox: {
      width : width,
     justifyContent:'center',
     alignItems:'center',
  },
  label1:{
   
    width: width * 0.91,
    flexDirection:'row',
  },
  labelText:{

  },
  labelStar:{
    color: colours.Red,
  },
  input1: {
      borderWidth: 1,
      borderColor: colours.Green,
      width: width * 0.9,
      backgroundColor: colours.White,
      margin: 10,
      height :50,
      borderRadius:8,
      paddingLeft:8,
    },
    button1 : {
    
      height:height*0.06,
      width: width * 0.8,
      justifyContent:'center',
      alignItems:'center',
      backgroundColor: colours.Green,
      alignSelf : 'center',
      marginVertical :30,
      borderRadius:8
    },
    buttonText:{
      marginRight:10,
      color : colours.White,
      fontSize : 20,
      marginBottom:4,
    },
    })