import {
  Button,
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Delete from 'react-native-vector-icons/AntDesign';
import Form from 'react-native-vector-icons/FontAwesome';
import Add from 'react-native-vector-icons/Octicons';
import Edit from 'react-native-vector-icons/Entypo';
import React from 'react';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useIsFocused} from '@react-navigation/native';
import {useSelector} from 'react-redux';
import Header from '../components/Header';
import {
  colours,
  BUDGET_TRACKER,
  GET_PROJECT_USERS,
  SALT,
  DELETE_FIXED_EXPENSES,
  DELETE_RECEIVED_PAYMENTS,
} from '../constants';
import {useEffect, useState} from 'react';
import {TextInput} from 'react-native-paper';
import DropDownPicker from 'react-native-dropdown-picker';
import {relativeTimeRounding} from 'moment';
import {useDispatch} from 'react-redux';
import {
  setExpenses,
  setExpensesID,
  setEditExpenses,
  setProjectUsers,
  setUser,
  setUserID,
  setEditPayments,
  setProjectID,
} from '../redux/slice/User';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import md5 from 'md5';
import axios from 'axios';
import moment from 'moment';
import {set} from 'immer/dist/internal';

const {width, height} = Dimensions.get('screen');

const BudgetTrackers = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const [loading, setLoading] = useState(false);
  const [fixedExpenses, setFixedExpenses] = useState();
  const [developerExpenses, setDeveloperExpenses] = useState();
  const [developerName, setDeveloperName] = useState();
  const [addedExpense, setAddedExpense] = useState();
  const [addPayments, setAddPayments] = useState();
  const projects = useSelector(state => state.user.projects);
  const projectIDDet = useSelector(state => state.user.projectID);
  console.log('projectthu', projectIDDet);
  const userID = useSelector(state => state.user.userID);
  const viewFixedExpenses = useSelector(state => state.user.fixedExpenses);
  const [date, setDate] = useState('projectspan');
  const [projectUserID, setProjectUserID] = useState([]);
  // const [userIDS, setUserIDS] = useState();
  const [devName, setDevName] = useState();
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isDatePickerVisible2, setDatePickerVisibility2] = useState(false);
  const [fromDate, setFromDate] = useState(' ');
  const [toDate, setToDate] = useState(' ');
  // const [dateSelector, setDateSelector] = useState(false);
  const [dateSelector1, setDateSelector1] = useState(false);
  const [userIDSDate, setUserIDSDate] = useState([]);
  const [dayType, setDayType] = useState('projectspan');
  const [blockFromDate, setBlockFromDate] = useState(true);
  const [blockToDate, setBlockToDate] = useState(true);
  const [showToDate, setShowToDate] = useState(false);
  const [addUserID, setAddUserID] = useState([]);
  const [addUserID2, setAddUserID2] = useState([]);
  console.log('adduserID', addUserID,addUserID2)
  // const projectID3 = useRoute().params.projectID2;
  // const projectName3 = useRoute().params.projectName3;
  console.log(dateSelector1);

  useEffect(() => {
    name();
    expenses();
  }, [isFocused]);

  const name = () => {
    // projects.map(item => {
    //   console.log('projectID', item.name, item.id_string);
    const projectID1 = projectIDDet;
    const receiveEnd = GET_PROJECT_USERS;
    const userID1 = userID;
    const projectID = projectID1;
    const auth_token = md5(SALT + userID1 + projectID);

    const data1 = {
      developer_id: userID1,
      project_id: projectID,
      auth_token: auth_token,
    };

    axios
      .post(receiveEnd, data1)
      .then(Response => {
        if (Response.data.success) {
          setItems(Response.data.parameters);
        } else {
          alert('name', Response.data.message);
        }
      })
      .catch(err => {
        console.log(err);
      });
    // });
  };

  const expenses = () => {
    setLoading(false);
    // projects.map(item => {
      // console.log('projectID', item.name, item.id_string);
      const projectID1 = projectIDDet;
      const receiveEnd2 = BUDGET_TRACKER;
      const userID1 = userID;
      const projectID = projectID1;
      const type = date;
      const auth_token2 = md5(SALT + userID1 + projectID + type);
      console.log('all', userID1, projectID, type);
      const data2 = {
        developer_id: userID1,
        project_id: projectID,
        type: type,
        auth_token: auth_token2,
      };
      console.log('data8 is', data2);
      axios
        .post(receiveEnd2, data2)
        .then(Response => {
          if (Response.data.success) {
            console.log('response is1', Response.data.parameters);

            if (Response.data.parameters.dev_expenses.length > 0) {
              setFixedExpenses(Response.data.parameters);
              setAddedExpense(Response.data.parameters.fixed_expenses);
              setAddPayments(Response.data.parameters.payments_received);
              setDeveloperExpenses(Response.data.parameters.dev_expenses);
              setDeveloperName(Response.data.parameters.dev_expenses[0].name);
              setLoading(true);
            } else {
              setFixedExpenses(Response.data.parameters);
              setAddedExpense(Response.data.parameters.fixed_expenses);
              setAddPayments(Response.data.parameters.payments_received);
              setDeveloperExpenses('');
              setDeveloperName('');
              setLoading(true);
            }
          } else {
            alert(Response.data.message);
          }
        })
        .catch(err => {
          alert(err);
          console.log('hi', err);
        });
    // });
  };

  const handleSubmitTest = data => {
    // if (data.length > 0) {
    //  if(adddrop){
      console.log('changedata',data.label)
      data.forEach(element => {
       let array = []
        console.log('foreach', element.label)
      
    console.log('dropvalue', data);
    setLoading(false);
    const userIDS = parseInt(data.value);
    const userName = element.label;
    
    const projectID = projectIDDet;
    const receiveEnd2 = BUDGET_TRACKER;
    const userID1 = userID
    const userIDS1 =userIDS
    const type = dayType;
  
    const auth_token2 = md5(SALT + userID1 + projectID + type);
    const data3 = {
      developer_id: userID1,
      project_id: projectID,
      type: type,
      user_ids: userIDS1,
      from_date: fromDate,
      to_date: toDate,
      auth_token: auth_token2,
    };
    console.log('data4', data3);
    // setAddDrop(false);

    axios
      .post(receiveEnd2, data3)
      .then(Response => {
        if (Response.data.success) {
          if (Response.data.parameters.dev_expenses.length > 0) {
            setFixedExpenses(Response.data.parameters);
            setDeveloperExpenses(Response.data.parameters.dev_expenses);
            setAddedExpense(Response.data.parameters.fixed_expenses);
            setAddPayments(Response.data.parameters.payments_received);
            console.log('result is1', Response.data.parameters,userName);

            if (developerName == userName) {
              console.log('if condition', developerName, userName);
              if (Response.data.parameters.dev_expenses.length > 0) {
                setDeveloperExpenses(Response.data.parameters.dev_expenses);
                setAddedExpense(Response.data.parameters.fixed_expenses);
                setAddPayments(Response.data.parameters.payments_received);
                setLoading(true);
              }
            } else {
              setDeveloperExpenses();
              setLoading(true);
            }
          } else {
            setDeveloperExpenses();
            setLoading(true);
          }
        } else {
          alert(Response.data.message);
        }
      })
      .catch(err => {
        console.log(err);
      });
      
    });
  };

  const handleSubmitSpan = dateSpan => {
    console.log('dateSpan', dateSpan);
    setDayType(dateSpan);
    setLoading(false);
    if (dateSpan == 'customview') {
      // if(loading=="true"){
      expenses();
      setDateSelector1(true);
      console.log('loading is ', loading);
      // }
      // else{
      //   setDateSelector1(false);
      // }
    }
    //     if (dateSpan == 'customview') {
    //       expenses();
    //       setDateSelector1(true);
    //       console.log('loading is ', loading)

    // }
    //    else {
    //     setDateSelector1(false);
    else {
      setDateSelector1(false);

      const userIDS1 = projectUserID;
      const userName = devName;

      const receiveEnd3 = BUDGET_TRACKER;
      const userID1 = userID;
      const projectID = projectIDDet
      const type = dateSpan;
      const userspanID = userIDS1;
      const auth_token2 = md5(SALT + userID1 + projectID + type);

      const data3 = {
        developer_id: userID1,
        project_id: projectID,
        type: type,
        user_ids: userspanID,
        auth_token: auth_token2,
      };
      console.log('dataspan2 is', data3, receiveEnd3);
      axios
        .post(receiveEnd3, data3)
        .then(Response => {
          if (Response.data.success) {
            setFixedExpenses(Response.data.parameters);
            console.log('resultspan2 is', Response.data);
            if (Response.data.parameters.dev_expenses.length > 0) {
              setDeveloperExpenses(Response.data.parameters.dev_expenses);
              setLoading(true);
            } else {
              setDeveloperExpenses();
              setLoading(true);
            }
          } else {
            alert(Response.data.message);
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  const editExpenses = expense2 => {
    dispatch(setEditExpenses(expense2));
    setValue([]);
    setValue2(null);
    navigation.navigate('UpdateFixedExpenses');
  };

  const editPayments = expense => {
    dispatch(setEditPayments(expense));
    setValue([]);
    setValue2(null);
    navigation.navigate('UpdateReceivedPayments');
  };

  const deleteExpenses = remove => {
    setLoading(false);
    setValue([]);
    setValue2(null);
    const receiveEndDelete = DELETE_FIXED_EXPENSES;
    const userID1 = userID;
    const deleteID = remove;
    const auth_token = md5(SALT + userID1 + deleteID);

    const deleteData = {
      developer_id: userID1,
      id: deleteID,
      auth_token: auth_token,
    };
    console.log('deleteData', deleteData);
    axios
      .post(receiveEndDelete, deleteData)
      .then(Response => {
        if (Response.data.success) {
          expenses();
        }
      })
      .catch(err => {
        alert(err);
      });
  };

  const deletePayments = removePaymentsID => {
    setValue([]);
    setValue2(null);
    const userID1 = userID;
    const deleteID = removePaymentsID;
    const deletePaymentsEnd = DELETE_RECEIVED_PAYMENTS;
    const auth_token = md5(SALT + userID1 + deleteID);

    const deletePaymentsdata = {
      developer_id: userID1,
      id: deleteID,
      auth_token: auth_token,
    };

    console.log('data2 is', deletePaymentsdata);
    axios
      .post(deletePaymentsEnd, deletePaymentsdata)
      .then(Response => {
        if (Response.data.success) {
          expenses();
        }
      })
      .catch(err => {
        alert(err);
      });
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
    setBlockFromDate(false);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  const handleConfirm = date => {
    const day1 = moment(date).format('YYYY-MM-DD');
    setFromDate(day1);
    setShowToDate(true);
    hideDatePicker();
  };
  const hideFromDate = () => {
    setDatePickerVisibility(false);
    setBlockFromDate(true);
  };

  const showDatePicker2 = () => {
    setDatePickerVisibility2(true);
    setBlockToDate(false);
  };
  const hideDatePicker2 = () => {
    setDatePickerVisibility2(false);
    setBlockToDate(false);
  };
  const hideToDate = () => {
    setDatePickerVisibility2(false);
    setBlockToDate(true);
  };

  const handleConfirm2 = date => {
    let day2 = moment(date).format('YYYY-MM-DD');
    setToDate(day2);
    hideDatePicker2();
    setLoading(false);
    setShowToDate(true);
    const userIDS1 = userIDSDate;
    console.log('label is ', userIDS1);
    // projects.map(item => {
    const receiveEnd2 = BUDGET_TRACKER;
    const userID1 = userID;
    const projectID = projectIDDet;
    const userspanID = userIDS1;
    const type = 'customview';
    const auth_token4 = md5(SALT + userID1 + projectID + type);

    const data4 = {
      developer_id: userID1,
      project_id: projectID,
      type: type,
      user_ids: userspanID,
      from_date: fromDate,
      to_date: day2,
      auth_token: auth_token4,
    };

    console.log('datepicker2', fromDate, toDate);

    axios
      .post(receiveEnd2, data4)
      .then(Response => {
        if (Response.data.success) {
          // console.log('datapicker1 is', data4);

          // elseif(fromDate<toDate){
          //   console.log('failed')
          // }
          setFixedExpenses(Response.data.parameters);
          console.log('datepicker result is', Response.data.parameters);
          if (Response.data.parameters.dev_expenses.length > 0) {
            setDeveloperExpenses(Response.data.parameters.dev_expenses);
            setLoading(true);
          } else {
            setDeveloperExpenses();
            setLoading(true);
          }
        } else {
          alert(Response.data.message);
        }
      })
      .catch(err => {
        console.log(err);
      });
    // });
  };

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState([]);
  const [items, setItems] = useState([]);

  const [open2, setOpen2] = useState(false);
  const [value2, setValue2] = useState(null);
  const [items2, setItems2] = useState([
    {label: 'PROJECT SPAN', value: 'projectspan'},
    {label: 'CURRENT WEEK', value: 'curweek'},
    {label: 'CURRENT MONTH', value: 'curmonth'},
    {label: 'TODAY', value: 'today'},
    {label: 'CUSTOM VIEW', value: 'customview'},
  ]);

  return (
    <ScrollView style={{flex: 1}}>
      <View>
        <View style={styles.Header}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('ListOfProjects');
            }}>
            <Icon
              name="arrowleft"
              style={styles.Arrow}
              size={40}
              color="#000000"
            />
          </TouchableOpacity>
          <View>
            <Text style={styles.HeaderText}>Budget Tracker</Text>
          </View>
        </View>

        {loading ? (
          <View style={styles.Filters}>
            <View style={styles.dropdownBox}>
              <DropDownPicker
                style={{...styles.ProjectList}}
                open={open}
                // value={items.map(item => (item.id))}
                value={value}
                items={items.map(item => ({label: item.name, value: item.id}))}
                setOpen={setOpen}
                setValue={setValue}
                setItems={setItems}
                placeholder="Select User"
                listMode="SCROLLVIEW"
                closeAfterSelecting={true}
                multiple={true}
                min={0}
                max={5}
                containerStyle={styles.dropdownContainer}
                 onSelectItem ={(value)=>{handleSubmitTest(value)}}
               




                  
                
              />
            </View>

            <View style={styles.dropdownBox}>
              <DropDownPicker
                style={styles.ProjectList}
                open={open2}
                value={value2}
                items={items2}
                setOpen={setOpen2}
                setValue={setValue2}
                setItems={setItems2}
                placeholder="Select date"
                listMode="SCROLLVIEW"
                containerStyle={styles.dropdownContainer}
                onSelectItem={value => handleSubmitSpan(value.value)}
              />
            </View>
          </View>
        ) : null}
        {dateSelector1 ? (
          loading ? (
            <View style={styles.datePicker}>
              <TouchableOpacity
                style={styles.touchDate}
                onPress={showDatePicker}>
                {blockFromDate ? (
                  <Text style={styles.datePickerText}>From date</Text>
                ) : (
                  <Text style={styles.datePickerText}>{fromDate}</Text>
                )}
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                onConfirm={handleConfirm}
                onCancel={hideFromDate}
              />

              <TouchableOpacity
                style={styles.touchDate}
                onPress={() => {
                  if (showToDate) {
                    showDatePicker2();
                  } else {
                    alert('select from date');
                    setLoading(true);
                  }
                }}>
                {blockToDate ? (
                  <Text style={styles.datePickerText}>To date</Text>
                ) : (
                  <Text style={styles.datePickerText}>{toDate}</Text>
                )}
              </TouchableOpacity>
              <DateTimePickerModal
                isVisible={isDatePickerVisible2}
                mode="date"
                onConfirm={handleConfirm2}
                onCancel={hideToDate}
                minimumDate={new Date(fromDate)}
              />
            </View>
          ) : (
            <View></View>
          )
        ) : (
          <View></View>
        )}

        {fixedExpenses ? (
          loading ? (
            <View style={styles.entireBox}>
              <View>
                {developerExpenses ? (
                  <View>
                    <View style={{...styles.nameCard, marginTop: 10}}>
                      <Text style={styles.nameCardText}>
                        Developer Expenses
                      </Text>
                    </View>

                    <View style={styles.expensesBox}>
                      <View style={styles.TextBox1}>
                        <Text style={styles.BudgetText1}>
                          {developerExpenses[0].name}
                        </Text>
                      </View>
                      <View style={styles.TextBox1}>
                        <Text style={styles.BudgetText1}>
                          {Math.round(developerExpenses[0].hrsTaken)}hrs
                        </Text>
                      </View>
                      <View style={styles.TextBox1}>
                        <Text style={styles.BudgetText1}>
                          ₹{Math.round(developerExpenses[0].amount)}
                        </Text>
                      </View>
                    </View>
                  </View>
                ) : (
                  <View></View>
                )}
              </View>

              <View style={styles.expensesCard}>
                <View style={styles.belowExpenses}>
                  <View style={{...styles.nameCard, marginTop: 20}}>
                    <Text style={styles.nameCardText}>Fixed Expenses</Text>
                  </View>

                  <View>
                    <TouchableOpacity
                      style={styles.addExpense}
                      onPress={() => {
                        navigation.navigate('AddFixedExpenses');
                        setValue([]);
                        setValue2(null);
                      }}>
                      <Add
                        name="diff-added"
                        size={16}
                        color={colours.White}
                        style={styles.addIcon}
                      />
                      <View style={styles.addExpensesBox}>
                        <Text style={styles.addExpensesText}>Add </Text>
                        <Text style={styles.addExpensesText}>Expenses</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              {addedExpense.length > 0 ? (
                addedExpense.map((item1, index) => {
                  return (
                    <View style={styles.expensesBox} key={index}>
                      <View style={styles.TextBox1}>
                        {item1.added_at ? (
                          <Text style={styles.BudgetText1}>
                            {item1.added_at}
                          </Text>
                        ) : (
                          <Text style={styles.BudgetText1}>--</Text>
                        )}
                      </View>

                      <View style={styles.TextBox1}>
                        {item1.amount ? (
                          <Text style={styles.BudgetText1}>
                            ₹{item1.amount}
                          </Text>
                        ) : (
                          <Text style={styles.BudgetText1}>--</Text>
                        )}
                      </View>
                      <View style={styles.TextBox1}>
                        {item1.amount ? (
                          <Text style={styles.BudgetText1} numberOfLines={1}>
                            ₹{item1.amount}
                          </Text>
                        ) : (
                          <Text style={styles.BudgetText1}>--</Text>
                        )}
                      </View>
                      <View style={styles.editButton}>
                        <TouchableOpacity
                          style={styles.touchButton}
                          onPress={() => editExpenses(item1)}>
                          <Edit name="edit" size={22} color={colours.Green} />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.touchButton}
                          onPress={() => deleteExpenses(item1.id)}>
                          <Delete name="delete" size={22} color={colours.Red} />
                        </TouchableOpacity>
                      </View>
                    </View>
                  );
                })
              ) : (
                <View
                  style={{
                    ...styles.expensesBox,
                    justifyContent: 'space-around',
                  }}>
                  <View style={styles.TextBox1}>
                    <Text style={styles.BudgetText1}>--</Text>
                  </View>

                  <View style={styles.TextBox1}>
                    <Text style={styles.BudgetText1}>--</Text>
                  </View>
                  <View style={styles.TextBox1}>
                    <Text style={styles.BudgetText1}>--</Text>
                  </View>
                  {/* <View style={styles.editButton}>
                <TouchableOpacity
                  style={styles.touchButton}
                  onPress={() => editExpenses(item1)}>
                  <Edit name="edit" size={22} color={colours.Green} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.touchButton}
                  onPress={() => deleteExpenses(item1.id)}>
                  <Delete name="delete" size={22} color={colours.Red} />
                </TouchableOpacity>
              </View> */}
                </View>
              )}

              <View style={styles.expensesCard}>
                <View style={styles.belowExpenses}>
                  <View style={{...styles.nameCard, marginTop: 20}}>
                    <Text style={styles.nameCardText}>Payments Received</Text>
                  </View>
                  <View>
                    <TouchableOpacity
                      style={styles.addExpense}
                      onPress={() => {
                        setValue([]);
                        setValue2(null);
                        navigation.navigate('AddPayments');
                      }}>
                      <Add
                        name="diff-added"
                        size={16}
                        color={colours.White}
                        style={styles.addIcon}
                      />
                      <View style={{...styles.addExpensesBox}}>
                        <Text style={styles.addExpensesText}>Add </Text>
                        <Text style={styles.addExpensesText}>Payments</Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              {addPayments.length > 0 ? (
                addPayments.map((item, index) => {
                  return (
                    <View style={styles.expensesBox} key={index}>
                      <View style={styles.TextBox1}>
                        <Text style={styles.BudgetText1}>{item.added_at}</Text>
                      </View>

                      <View style={styles.TextBox1}>
                        <Text style={styles.BudgetText1}>₹{item.amount}</Text>
                      </View>
                      <View style={styles.TextBox1}>
                        <Text style={styles.BudgetText1}>₹{item.amount}</Text>
                      </View>
                      <View style={styles.editButton}>
                        <TouchableOpacity
                          style={styles.touchButton}
                          onPress={() => editPayments(item)}>
                          <Edit name="edit" size={22} color={colours.Green} />
                        </TouchableOpacity>
                        <TouchableOpacity
                          style={styles.touchButton}
                          onPress={() => deletePayments(item.id)}>
                          <Delete name="delete" size={22} color={colours.Red} />
                        </TouchableOpacity>
                      </View>
                    </View>
                  );
                })
              ) : (
                <View
                  style={{
                    ...styles.expensesBox,
                    justifyContent: 'space-around',
                  }}>
                  <View style={styles.TextBox1}>
                    <Text style={styles.BudgetText1}>--</Text>
                  </View>

                  <View style={styles.TextBox1}>
                    <Text style={styles.BudgetText1}>--</Text>
                  </View>
                  <View style={styles.TextBox1}>
                    <Text style={styles.BudgetText1}>--</Text>
                  </View>
                </View>
              )}

              <View style={styles.BudgetBox}>
                <View style={styles.expensesCard}>
                  <View style={{...styles.nameCard, marginTop: 20}}>
                    <Text style={styles.nameCardText}>Total</Text>
                  </View>
                </View>
                <View style={styles.BudgetBox1}>
                  <View style={styles.TextBox}>
                    <Text style={styles.BudgetText}>Total Hours </Text>
                    <Text style={styles.BudgetText}>Total Expenses</Text>
                    <Text style={styles.BudgetText}>Budget </Text>
                    <Text style={styles.BudgetText}>Profit </Text>
                    <Text style={styles.BudgetText}>%</Text>
                    <Text style={styles.BudgetText}>Payments Received </Text>
                    <Text style={styles.BudgetText}>Payments Pending </Text>
                    <Text style={styles.BudgetText}>
                      Payment Received as %{' '}
                    </Text>
                  </View>

                  <View style={styles.TextBox}>
                    <Text style={styles.TextColon}>:</Text>
                    <Text style={styles.TextColon}>:</Text>
                    <Text style={styles.TextColon}>: </Text>
                    <Text style={styles.TextColon}>:</Text>
                    <Text style={styles.TextColon}>: </Text>
                    <Text style={styles.TextColon}>: </Text>
                    <Text style={styles.TextColon}>:</Text>
                    <Text style={styles.TextColon}>: </Text>
                  </View>

                  <View style={styles.TextBox}>
                    <Text style={styles.TextValue}>
                      {Math.round(fixedExpenses.total_hours)}
                    </Text>
                    <Text style={styles.TextValue}>
                      ₹{Math.round(fixedExpenses.total_amount)}
                    </Text>
                    <Text style={styles.TextValue}>
                      {fixedExpenses.budget_value}
                    </Text>
                    <Text style={styles.TextValue}>
                      {Math.round(fixedExpenses.profit)}
                    </Text>
                    <Text style={styles.TextValue}>
                      {Math.round(fixedExpenses.profit_percentage)}
                    </Text>
                    <Text style={styles.TextValue}>
                      {Math.round(fixedExpenses.total_payments_received)}
                    </Text>
                    <Text style={styles.TextValue}>
                      {Math.round(fixedExpenses.payments_pending)}
                    </Text>
                    <Text style={styles.TextValue}>
                      {fixedExpenses.percentage_based_on_payments}
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          ) : (
            <View style={styles.loader}>
              <View style={styles.loaderCard}>
                <ActivityIndicator size="large" color={colours.Green} />
                <Text>Loading</Text>
              </View>
            </View>
          )
        ) : (
          <View style={styles.loader}>
            <View style={styles.loaderCard}>
              <ActivityIndicator size="large" color={colours.Green} />
              <Text>Loading</Text>
            </View>
          </View>
        )}
      </View>
    </ScrollView>
  );
};

export default BudgetTrackers;

const styles = StyleSheet.create({
  BudgetBox: {
    marginTop: 20,
  },
  loader: {
    height: height,
    width: width,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loaderCard: {
    height: height * 0.1,
    width: width * 0.2,
    borderWidth: 1,
    borderColor: colours.Black,
    backgroundColor: colours.White,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 100,
  },
  BudgetBox1: {
    height: height * 0.5,
    width: width * 0.97,
    shadowColor: colours.Black,
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.1,
    borderWidth: 1,
    flexDirection: 'row',
    backgroundColor: colours.White,
    alignSelf: 'center',
  },
  main: {
    width: width,
    alignItems: 'center',
    height: height,
    justifyContent: 'center',
    alignContent: 'center',
  },
  entireBox: {
    width: width,
    alignItems: 'center',
    justifyContent: 'center',
  },
  BudgetText: {
    marginHorizontal: 10,
    marginTop: 25,
    color: colours.Black,
    fontWeight: 'bold',
    width: width * 0.41,
    fontSize: 14,
  },
  TextColon: {
    marginHorizontal: 10,
    marginTop: 25,
    color: colours.Black,
    fontWeight: 'bold',
    width: width * 0.02,
    marginLeft: 10,
    fontSize: 14,
  },
  TextValue: {
    marginHorizontal: 10,
    marginTop: 25,
    color: colours.Black,
    fontWeight: 'bold',
    width: width * 0.22,
    textAlign: 'center',
    fontSize: 14,
  },
  TextBox: {
    alignItems: 'center',
    marginLeft: 9,
  },
  Header: {
    height: height * 0.07,
    width: width,
    backgroundColor: colours.Green,
    flexDirection: 'row',
    alignItems: 'center',
  },
  HeaderText: {
    fontSize: 17,
    fontWeight: '600',
    color: colours.White,
    marginLeft: 80,
  },
  Arrow: {
    padding: 10,
    color: colours.White,
  },
  form: {
    paddingLeft: 70,
  },
  dropdownBox: {
    width: width * 0.46,
    marginTop: 40,
    marginBottom: 10,
  },
  ProjectList: {
    width: width * 0.44,
    height: 20,
    marginBottom: 50,
  },
  dropdownContainer: {
    width: width * 0.44,
  },
  Filters: {
    height: height * 0.1,
    width: width,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  new: {
    height: height,
    width: width,
    alignItems: 'center',
  },
  expensesBox: {
    height: height * 0.07,
    width: width * 0.97,
    shadowColor: '#000000',
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 0.1,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    alignSelf: 'center',
    borderWidth: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: 3,
    paddingLeft: 3,
  },
  loaderBox: {
    height: height * 0.5,
    width: width * 0.9,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
  },

  expensesBox1: {
    height: 10,
    width: width * 0.2,
    borderWidth: 1,
  },
  TextBox1: {},
  BudgetText1: {
    color: '#000000',
    fontWeight: 'bold',
    fontSize: 15,
  },
  BudgetText2: {
    color: colours.Black,
    fontWeight: 'bold',
    width: 120,
    fontSize: 14,
  },

  ButtonBox: {
    height: height * 0.2,
    width: width,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    width: width * 0.35,
    marginVertical: 10,
  },

  expensesCard: {
    width: width * 0.97,
    alignSelf: 'center',
  },
  belowExpenses: {
    flexDirection: 'row',
    width: width * 0.97,
    justifyContent: 'space-between',
  },
  addExpensesBox: {
    marginRight: 3,
  },
  addExpensesText: {
    fontSize: 10,
    color: colours.White,
    textAlign: 'center',
    fontWeight: '700',
  },
  addIcon: {
    margin: 4,
  },
  nameCard: {
    height: height * 0.05,
    borderWidth: 1,
    backgroundColor: '#4285f4',
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.5,
  },
  nameCardText: {
    color: colours.White,
    fontSize: 17,
    textAlign: 'center',
    fontWeight: '600',
  },
  addExpense: {
    height: height * 0.04,
    backgroundColor: '#5cb85c',
    marginTop: 28,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 1,
  },
  editButton: {
    flexDirection: 'row',
    paddingTop: 0,
  },
  touchButton: {
    paddingLeft: 3,
  },

  datePicker: {
    height: height * 0.1,
    width: width * 0.98,
    color: colours.Green,
    alignSelf: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  touchDate: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: colours.Black,
    width: width * 0.44,
    height: 50,
    backgroundColor: colours.White,
    margin: 10,
    justifyContent: 'center',
    paddingLeft: 6,
  },
  datePickerText: {},
});
