import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TextInput,
  Touchable,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import Header from '../components/Header';
import {colours, ADD_FIXED_EXPENSES, SALT} from '../constants';
import {useState, useEffect} from 'react';
import {Button} from 'react-native-paper';
import {useSelector, useDispatch} from 'react-redux';
import md5 from 'md5';
import axios from 'axios';
import {setFixedExpenses} from '../redux/slice/User';
import AsyncStorage from '@react-native-async-storage/async-storage';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;
const AddFixedExpenses = () => {
  const user = useSelector(state => state.user.parameters);
  const userID = useSelector(state => state.user.userID);
  console.log('userid is ', userID);
  const projects = useSelector(state => state.user.projects);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [purpose, setPurpose] = useState('');
  const [amount, setAmount] = useState('');
  const [addExpenses, setAddExpenses] = useState();
  const [message, setMessage] = useState();
  const [data, setData] = useState([]);
  const [store1, setStore1] = useState();
  const [purposeError, setPurposeError] = useState(false);
  const [amountError, setAmountError] = useState(false);
  const [amountError1, setAmountError1] = useState(false);
  const [amountText, setAmountText] = useState();
  const [alertText, setAlertText] = useState();
  console.log('getdata now  is', data);
  const patternAmount = /^[0-9]{1,6}$/;
  const decimalAmount = /^[0-9]$/;

  const handleSubmit = () => {
    if (purpose === '') {
      setPurposeError(true);
    } else if (amount === '') {
      setAmountError(false);
      setAmountError1(true);
      setAmountText('Please add amount*');
    } else if (patternAmount.test(amount)) {
      projects.map(item => {
        console.log('projectID', item.name, item.id_string);
        const projectID = item.id_string;
        const receiveEnd = ADD_FIXED_EXPENSES;
        const userID1 = userID;
        const auth_token = md5(SALT + userID + projectID + purpose);

        const data1 = {
          developer_id: userID1,
          project_id: projectID,
          purpose: purpose,
          amount: amount,
          auth_token: auth_token,
        };
        console.log(' fixed data1 is', data1);
        axios
          .post(receiveEnd, data1)
          .then(Response => {
            if (Response.data.success) {
              dispatch(setFixedExpenses(Response.data.parameters));
              setAddExpenses(Response.data.parameters);
              console.log('added expenses is', Response.data.parameters);
              setStore1(Response.data.parameters.id);
              navigation.navigate('BudgetTrackers');
            } else {
              alert('Please fill mandatory fields', Response.data.message);
            }
          })
          .catch(err => {
            console.log(err);
          });
      });
    } else {
      if(decimalAmount.test(amount)){
        setAmount('');
      alert('maximum limit below 10lacs and value should be 0-9');
      // alert('Decimal value not allowed')
      }else{
        setAmount('');
      alert('Decimal value not allowed');
      }
      
    }
  };

  return (
    <ScrollView>
      <Header name="Add Fixed Expenses" />
      <View style={styles.main}>
        <View style={styles.formBox}>
          <View style={styles.inputBox}>
            <View style={styles.label1}>
              <Text style={styles.labelText}>Purpose</Text>
              <Text style={styles.labelStar}>*</Text>
            </View>
            <TextInput
              placeholder="Purpose"
              placeholderTextColor={colours.Green}
              style={styles.input1}
              keyboardType="default"
              onChangeText={value => {
                setPurpose(value);
                
                setPurposeError(false);
              }}></TextInput>
            {purposeError ? (
              <View style={styles.label1}>
                <Text style={{...styles.labelText, color: colours.Red}}>
                  Please enter purpose*
                </Text>
              </View>
            ) : null}

            <View style={styles.label1}>
              <Text style={styles.labelText}>Amount</Text>
              <Text style={styles.labelStar}>*</Text>
            </View>
            <TextInput
            value={amount}
              placeholder="Amount"
              placeholderTextColor={colours.Green}
              style={styles.input1}
              keyboardType="number-pad"
              onChangeText={value => {
                setAmount(value);
                if(value.length>6){
                setAmountError(true);}
                else{
                  setAmountError(false);
                }
                setAmountError1(false);
              }}></TextInput>
            {amountError ? (
              <View style={styles.label1}>
                <Text style={{...styles.labelText, color: colours.Red}}>
                  maximum limit is below 10lac
                </Text>
              </View>
            ) : null}
            {amountError1 ? (
              <View style={styles.label1}>
                <Text style={{...styles.labelText, color: colours.Red}}>
                  {amountText}
                </Text>
              </View>
            ) : null}
          </View>

          <TouchableOpacity
            onPress={() => {
              handleSubmit();
            }}
            style={styles.button1}>
            <Text style={styles.buttonText}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

export default AddFixedExpenses;

const styles = StyleSheet.create({
  main: {
    height: height,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colours.White,
  },
  formBox: {
    height: height * 0.4,
    width: width,
    backgroundColor: colours.White,
  },
  inputBox: {
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label1: {
    width: width * 0.91,
    flexDirection: 'row',
  },
  labelText: {},
  labelStar: {
    color: colours.Red,
  },
  input1: {
    borderWidth: 1,
    borderColor: colours.Green,
    width: width * 0.9,
    backgroundColor: colours.White,
    margin: 10,
    height: 50,
    borderRadius: 8,
    paddingLeft: 8,
  },
  button1: {
    height: height * 0.06,
    width: width * 0.8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colours.Green,
    alignSelf: 'center',
    marginVertical: 30,
    borderRadius: 8,
  },
  buttonText: {
    marginRight: 10,
    color: colours.White,
    fontSize: 20,
    marginBottom: 4,
  },
});
