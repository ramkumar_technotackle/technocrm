import { StyleSheet, Text, View,Dimensions ,Image} from 'react-native'
import React, { useEffect, useState } from 'react'
import { useNavigation } from '@react-navigation/native';
import { colours } from '../constants';
import AsyncStorage from '@react-native-async-storage/async-storage';



const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const SplashScreen = () => {
    const navigation = useNavigation();



  return (
    <View  >
     <View  style= {styles.main}>
            <View style={styles.photo}>
              <View>
                <Image
                  style={styles.image}
                  source={require('../assets/Images/TechnoLogo.png')}
                />
              </View>
            </View>
            <Text style={styles.HeadText}>TECHNO TACKLE</Text>
            <Text style={styles.HeadText2}>Software Solutions,Coimbatore</Text>
          </View>
    </View>
  )
}

export default SplashScreen

const styles = StyleSheet.create({

    head: {
        height: height,
        width: width,
        backgroundColor: colours.White,
        alignItems: 'center',
      },
    
      photo: {
        height: height * 0.15,
        width: width,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
      },
      HeadText: {
        color: colours.Green,
        marginTop: 20,
        width: width,
        fontSize: 25,
        textAlign: 'center',
        fontWeight: '500',
      },
      HeadText2: {
        color: colours.Green,
        fontSize: 14,
        textAlign: 'center',
      },
      image :{
        height: 140, 
        width: 165, 
        marginTop: 20
    },
    main : {
        height:height,
        width : width,
        justifyContent:'center',
        backgroundColor : colours.White,
    }

})