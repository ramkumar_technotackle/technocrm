
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import React from 'react';
import {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import Header from '../components/Header';
import {useSelector} from 'react-redux';
import {useDispatch} from 'react-redux';
import {Item} from 'react-native-paper/lib/typescript/components/List/List';
import {colours, GET_PROJECT_USERS, SALT} from '../constants';
import Icon from 'react-native-vector-icons/Ionicons';
import md5 from 'md5';
import axios from 'axios';
// import {setProjectUsers} from '../redux/slice/User';
 import {setProjectID} from '../redux/slice/User'
import { ReactNativeFirebase } from '@react-native-firebase/app';
const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const ListOfProjects = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const user1 = useSelector(state => state.user.parameters);
  const projects = useSelector(state => state.user.projects);
  const [list, setList] = useState();
  const [loading, setLoading] = useState(false);

  return (

      <View style={styles.main}>
        <Header name="Project List" />
        {projects ? (
          
          <FlatList
            nestedScrollEnabled={true}
            data={projects}
            renderItem={({item}) => (
              <TouchableOpacity
                style={styles.Project}
                onPress={() => {
                  // navigation.navigate('BudgetTrackers');
                  navigation.navigate("BudgetTrackers",{projectID2:item.id_string},{projectName2:item.name} )
                  console.log('projectis',item.id_string)
                  dispatch(setProjectID(item.id_string));
                }}>
                <View>
                  <Icon
                    name="cube"
                    style={styles.Arrow}
                    size={20}
                    color="#000000"
                  />
                </View>
                <View style={styles.Project1}>
                  <Text style={styles.ProjectName}>{item.name}</Text>
                </View>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.name}></FlatList>
           
        ) : (
          <View>
            <ActivityIndicator size="large" color="#00ff00" />
          </View>
        )}
      </View>
   
  );
};

export default ListOfProjects;

const styles = StyleSheet.create({
  main: {
    width: width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Project1: {
    width: width * 0.73,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Project: {
    height: height * 0.12,
    width: width * 0.9,
    marginTop: 10,
    alignContent: 'center',
    shadowColor: colours.Green,
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 1,
    elevation: 4,
    flexDirection: 'row',
    borderWidth: 0.1,
    alignItems: 'center',
  },
  ProjectName: {
    color: colours.Black,
    fontSize: 16,
    textAlign: 'center',
    paddingBottom: 5,
  },
  Arrow: {
    alignSelf: 'center',
    padding: 15,
    height: 50,
    width: 50,
    color: 'white',
    borderRadius: 30,
    marginLeft: 13,
    backgroundColor: colours.Green,
    marginTop: 1,
  },
});
