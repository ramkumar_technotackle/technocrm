import { StyleSheet, Text, View,ScrollView ,Dimensions, TouchableOpacity} from 'react-native'
import React  from 'react';
import { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import Header from '../components/Header';
import {useSelector} from 'react-redux';
import md5 from 'md5';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { setUser } from '../redux/slice/User';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;


const base_URL_Projects = 'js'
const List = () => {
  const SALT = 'TTCRM@2020'
  const navigation = useNavigation();
  const user = useSelector(state => state.user.parameters);
  const [loading,setLoading] = useState();
  const [details,setDetails] = useState();

  const dispatch = useDispatch();

const handleSubmit =() =>{
  setLoading(true)
  console.log('userdate', user.id);
  const auth_token = md5(SALT+user.id);
  const receiveEnd = base_URL_Projects;
  const data1 = {
    developer_id: user.id,
    auth_token: auth_token,
  };


  axios
        .post(receiveEnd, data1)
        .then(Response => {
        
          if(Response.data.success){
            setLoading(false)
            dispatch(setUser(Response.data.parameters))
          setDetails(Response.data);
          console.log('response data is', Response.data);
          navigation.navigate('ListofProjects')
        }
        else{
          setLoading(false)
          alert(Response.data.message)
        }
        })
        .catch(err => {
          console.log(err);
        });
}

  return (
    <ScrollView>
      <Header name="LIST"/>   
       <View  style ={styles.Main}>
 <View>
  <TouchableOpacity style={styles.ListDetails}  onPress={handleSubmit} >
<Text  style ={styles.ListText}> List Of Developers</Text>
  </TouchableOpacity>

  <TouchableOpacity style={styles.ListDetails} >
<Text  style ={styles.ListText}> List Of Leave Requests</Text>
  </TouchableOpacity>

  <TouchableOpacity style={styles.ListDetails} >
<Text  style ={styles.ListText}> List Of  Month Wise Leave Requests</Text>
  </TouchableOpacity>

  <TouchableOpacity style={styles.ListDetails} >
<Text  style ={styles.ListText}> List Of Projects</Text>
  </TouchableOpacity>
 </View>
    </View>
    </ScrollView>

  )
}

export default List

const styles = StyleSheet.create({
Main :{
  height: height,
  width : width,
  backgroundColor: 'skyblue',  
},

ListDetails:{
  height : 60,
  width : width,
  borderBottomWidth : 1,
  alignItems : 'center',
  justifyContent: 'center'
},
ListText : {
  fontSize : 18,
  fontWeight :'600',

}
})
