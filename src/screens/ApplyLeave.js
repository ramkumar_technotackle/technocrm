import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Dimensions,
  TextInput,
  TouchableOpacity,
  Button,
  ActivityIndicator,
} from 'react-native';
import {Dropdown} from 'react-native-element-dropdown';
import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import moment from 'moment';
import DropDownPicker from 'react-native-dropdown-picker';
import md5 from 'md5';
import axios from 'axios';
import Header from '../components/Header';
import { colours } from '../constants';
import { useSelector } from 'react-redux';
import { APPLY_LEAVE } from '../constants';
import { setUserID } from '../redux/slice/User';


const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;



const ApplyLeave = () => {
  const Navigation = useNavigation();
  const SALT = 'TTCRM@2020';
  const user =useSelector (state=> state.user.parameters)
  const userID =useSelector(state=> state.user.userID)
  const [leaveDate1,setLeaveDate1]=useState(false)
  const[hideTime,setHideTime] = useState('')
  const [devid, setDevid] = useState('');
  const [devComments, setDevComments] = useState('');
  const [token, setToken] = useState();
  const [completeData, setCompleteData] = useState('');
  const[loading,setLoading] = useState(true)
  const[blockFromDate,setBlockFromDate]= useState(true);
  const[blockFromTime,setBlockFromTime]= useState(true);
  const[blockToTime,setBlockToTime]= useState(true);
  let getDate = moment().format('MM/DD/YY');
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [isToTimePickerVisible, setToTimePickerVisibility] = useState(false);
  const [leaveDate, setLeaveDate] = useState("Date of leave");
  const [fromTime, setFromTime] = useState('From time');
  const [toTime, setToTime] = useState('To time');
  const [leaveDrop,setleaveDrop] = useState("Leave Type")
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const leaveType = value;
  console.log('holiday', leaveType);
  const lastmonth = new Date();
  lastmonth.setDate(lastmonth.getDate() - 30);

  const [items, setItems] = useState([
    {label: 'FULL DAY', value: 'FULL DAY'},
    {label: 'HALF DAY', value: 'HALF DAY'},
    {label: '2 hrs Permission', value: '2 hrs'},
    {label: 'Compensation Full Day', value: 'Compensation Full Day'},
    {label: 'Compensation Half Day', value: 'Compensation Half Day'},
    {label: 'Work From Home', value: 'Work From Home'},
  ]);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
    setBlockFromDate(false);
  };
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };
  const handleConfirm = date => {
    const day = moment(date).format('YYYY-MM-DD');
    setLeaveDate(day);
    hideDatePicker();
  };

  const showTimePicker = () => {
    setTimePickerVisibility(true);
    setBlockFromTime(false)
  };

  const handleConfirm1 = time => {
    const startTime = moment(time).format('HH:mm:ss');
    setFromTime(startTime);
    hideTimePicker();
  };

  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  };

  const showToTimePicker = () => {
    setToTimePickerVisibility(true);
    setBlockToTime(false)
  };

  const handleConfirm2 = time => {
    const endTime = moment(time).format('HH:mm:ss');
    setToTime(endTime);
    hideToTimePicker();
  };

  const hideToTimePicker = () => {
    setToTimePickerVisibility(false);
  };

  const handleSubmit = () => {
    setLoading(false);
    console.log('from is',fromTime);
    console.log('to time ', toTime)
    const userID1 = userID
    const auth_token = md5(SALT + userID + leaveType + leaveDate);
    console.log('all', SALT, userID, leaveType, leaveDate);
    const receiveEnd = APPLY_LEAVE;
    console.log('auth token', auth_token);
    const leaveData = {
      developer_id: userID1,
      leave_type: leaveType,
      date_of_leave: leaveDate,
      dev_comments: devComments,
      from_time: fromTime,
      to_time: toTime,
      auth_token: auth_token,
    };
    console.log('leaveData', leaveData);
  
 if(hideTime == "2 hrs"){
  if(fromTime!=="From time" && toTime!=="To time" && leaveDate!== "Date of leave"){
    if(fromTime<toTime){
      console.log('minus', toTime-fromTime)
    axios
      .post(receiveEnd, leaveData)
      .then(Response => {
        console.log('leave data', Response.data);

        if (Response.data.success) {
          setCompleteData(Response.data);
          setLoading(true);
          setValue(leaveDrop)
          setLeaveDate("Date of leave")
          setFromTime("From time")
        setToTime("To time")
        alert(Response.data.message)
          console.log('leave message', Response.data)
        } else {
          console.log('alert',Response.data.message)
          alert(Response.data.message)
          setLoading(true)
          
        }
      })

      .catch(err => {
        console.log(err);
        

      });
    }   
    }   else {
      alert ('difference between from and to time should be 2 hours')
      setLoading(true)
    }
  }
    else{
      if(leaveDate!=="Date of leave"){
        axios
      .post(receiveEnd, leaveData)
      .then(Response => {
        console.log('leave data', Response.data);

        if (Response.data.success) {
          setCompleteData(Response.data);
          setLoading(true);
          alert('Applied leave successfully');
          setValue(leaveDrop)
          setLeaveDate("Date of leave")
          setFromTime("From time")
        setToTime("To time")
          console.log('leave message', Response.data)
        }

         else {
          alert(Response.data.message);
          
          setLoading(true)
          
        }
      })

      .catch(err => {
        console.log('whats error',err);
        alert(err)
        

      });}
      else{
        alert('please fill mandatory details')
        setLoading(true)
      }
    }
  
    }


  return (
    <View>
      <Header name = "APPLY LEAVE" />
      {loading ? <View style={styles.main}>
        <View style={styles.form} >
          {/* <TextInput
            placeholder="developer ID"
            placeholderTextColor="#01b298"
            style={styles.input1}
            onChangeText={value => {
              setDevid(value);
            }}></TextInput> */}
           <View style={styles.labelBox}>
                <Text style={styles.labelText}>Leave Type</Text>
                <Text style={styles.star}>*</Text>
              </View>
          <DropDownPicker
            style={styles.input2}
            open={open}
            value={value}
            items={items}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
            placeholder={leaveDrop}
           textStyle={{color:colours.Green}}
            containerStyle= {styles.dropdownContainer}
            onChangeValue={(value)=>{setHideTime(value)}}
          />
          

          <View>
          <View style={styles.labelBox}>
                <Text style={styles.labelText}>Date of Leave</Text>
                <Text style={styles.star}>*</Text>
              
              </View>
            <TouchableOpacity
              style={styles.TouchInput}
              onPress={showDatePicker}>
              {blockFromDate? <Text style={styles.leaveText} >
              {leaveDate}
              </Text> : <Text style={styles.datePickerText}>
              {leaveDate}
              </Text> }
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isDatePickerVisible}
              mode="date"
              onConfirm={handleConfirm}
              onCancel={hideDatePicker}
              minimumDate={new Date()}
              
              
            />
          </View>


          <View style={styles.labelBox}>
                <Text style={styles.labelText}>Comments</Text>
                <Text style={styles.star}>*</Text>
              </View>
          <TextInput
            placeholder="Comments"
            placeholderTextColor={colours.Green}
            style={styles.input1}
            onChangeText={value => {
              setDevComments(value);
            }}></TextInput>


            { hideTime=="2 hrs"||hideTime=="HALF DAY" || hideTime == "Compensation Half Day" ?
            <>
<View style={styles.timeLabelBox}  >
<View style={styles.timeBox}>
                <Text style={styles.labelText}>From Time</Text>
                <Text >:</Text>
              </View>
              <View style={{...styles.timeBox,width:width*0.429,marginLeft:17, }}>
                <Text style={styles.labelText}>To Time</Text>
                <Text>:</Text>
              </View>
              </View>
              
          <View style={styles.timePicker}>
            <TouchableOpacity
              style={{...styles.TouchInput, width: width * 0.42}}
              onPress={showTimePicker}>
               {blockFromTime? <Text style={styles.leaveText}>
               {fromTime}
              </Text> : <Text style={styles.datePickerText}>
              {fromTime}
              </Text> }
            </TouchableOpacity>
            <DateTimePickerModal
              isVisible={isTimePickerVisible}
              mode="time"
              locale="en_GB"
              onConfirm={handleConfirm1}
              onCancel={hideTimePicker}


            />

            <TouchableOpacity
              style={{...styles.TouchInput, width: width * 0.429,}}
              onPress={showToTimePicker}>
               {blockToTime? <Text style={styles.leaveText}>
                {toTime}
              </Text> : <Text style={styles.datePickerText}>
              {toTime}
              </Text> }
            </TouchableOpacity>
            <DateTimePickerModal
            
              isVisible={isToTimePickerVisible}
              mode="time"
              onConfirm={handleConfirm2}
              onCancel={hideToTimePicker}
            />


          </View> 
          </> :null}


         
            <TouchableOpacity  style={styles.button1}  onPress={handleSubmit}>
              <Text
                style={{color: '#ffffff', fontSize: 20}}>
                APPLY LEAVE
              </Text>
            </TouchableOpacity>
  
        </View>
      </View> : <View style={styles.loader} ><View style={styles.loaderCard} >
            <ActivityIndicator size="large" color={colours.Green} />
            <Text  >Loading</Text>
          </View>
          </View> }
    </View>
  );
};

export default ApplyLeave;

const styles = StyleSheet.create({
  main: {
    height: height,
    width: width,
    backgroundColor: colours.White,
    alignItems:'center',
  },

  input1: {
    borderWidth: 1,
    borderColor: colours.Green,
    width: width * 0.9,
    backgroundColor: colours.White,
    paddingLeft:10,
    height:70,
    borderRadius:10,
    marginBottom :10,
  },
  input2: {
    borderWidth: 1,
    borderColor: colours.Green,
    width: width * 0.9,
    height: 50,
    backgroundColor: colours.White,
   marginBottom:12,
  },
  placeholderStyle: {
    fontSize: 15,
  },
  selectedTextStyle: {
    fontSize: 15,
  },
  TouchInput: {
    borderWidth: 1,
    borderColor: colours.Green,
    width: width * 0.9,
    height: 50,
    backgroundColor: colours.White,
    margin: 10,
    marginTop:0,
    justifyContent: 'center',
    paddingLeft:10,
    borderRadius:10,
  },
  timePicker: {
    flexDirection: 'row',
  
 
  
  },
  button1: {
    padding: 10,
    width: width * 0.8,
    alignItems: 'center',
    backgroundColor: colours.Green,
    marginTop: 30,
    borderRadius : 8,
  },
  Button2: {
    color: 'yellow',
    height: height,
  },
  dropdownContainer:{
    width:width*0.9,
    borderWidt:1,
    

  },
  form:{
    width:width,
    alignItems:'center',
    marginTop:20,
  },
  labelText: {
    alignSelf: 'flex-start',
  },
  labelBox: {
    alignSelf: 'center',
    marginBottom: 10,
    flexDirection: 'row',
    width:width*0.9,
    
   
  },
  star: {
    color: colours.Red,
  },
  timeLabelBox:{
    width:width*0.958,
    flexDirection:'row',
    
  },
  timeBox:{
    alignSelf: 'center',
    marginBottom: 10,
    flexDirection: 'row',
    alignSelf:'flex-start',
    width:width*0.42,
    marginLeft:13
  },
  loader:{
    height:height,
    width:width,
    alignItems:'center',
    justifyContent:'center',
      },

      loaderCard:{
        height : height*0.1,
        width:width*0.2,
        borderWidth:1,
        borderColor : colours.Black,
        backgroundColor:colours.White,
        alignItems:'center',
        justifyContent:'center',
        marginBottom:100,
      },
      leaveText:{
        color:colours.Green,
      }
});
