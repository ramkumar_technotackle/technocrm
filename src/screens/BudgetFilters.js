import { StyleSheet, Text, View, ScrollView, Dimensions, Button} from 'react-native'
import React from 'react'
import { useState } from 'react';
import { useNavigation } from '@react-navigation/native'
import Header from '../components/Header';
import { colours } from '../constants';
import DropDownPicker from 'react-native-dropdown-picker';
import { useSelector } from 'react-redux';


const height = Dimensions.get('window').height;
const width = Dimensions . get('window').width;
const BudgetFilters = () => {
    const user1 = useSelector(state => state.user.parameters);
    const projects = useSelector(state => state.user.projects);
    console.log('projects is ', projects)
    const projectUsers = useSelector(state => state.user.projectUsers)
    const navigation = useNavigation()
    const [open, setOpen] = useState(false);
  const [value, setValue] = useState(null);
  const [items,setItems]  =useState();
  const [open1, setOpen1] = useState(false);
  const [value1, setValue1] = useState(null);
  const [items1,setItems1]  =useState();
  const [open2, setOpen2] = useState(false);
  const [value2, setValue2] = useState(null);
  

  const [items2,setItems2]  =useState([
    {label: 'CURRENT WEEK', value: 'CURRENT WEEK'},
    {label: 'CURRENT MONTH', value: 'CURRENT MONTH'},
    {label: 'TODAY', value: 'TODAY'},
    {label: 'CUSTOM VIEW', value: 'CUSTOM VIEW'},
   
  ]);
 




//   const handleSubmit = () => {
//     projectUsers.map(item => {
//       console.log('ProjectUsers', item.name);
//     });
//   };

//   const handleSubmit1 = () => {
//     projectUsers.map(item => {
//       console.log('ProjectUsers', item.name);
//     });
//   };









  return (
  <ScrollView>
    <Header name="SELECT" />
    <View style= {styles.main} >
        <View style={styles.Filters}>
        <DropDownPicker
            style={styles.ProjectList}
            open={open}
            value={value}
            items={projects.map((item)=>({label : item.name,
            value : item.id_string}))}
            setOpen={setOpen}
            setValue={setValue}
            setItems={setItems}
            placeholder="Projects"
            listMode = "SCROLLVIEW"
            containerStyle={styles.DropdownContainer}
          />

<DropDownPicker
            style={styles.ProjectList}
            open={open1}
            value={value1}
            items={projectUsers.map((item)=>({label : item.name,
            value : item.name}))}
            setOpen={setOpen1}
            setValue={setValue1}
            setItems={setItems1}
            placeholder="Select User"
            listMode = "SCROLLVIEW"
            containerStyle={styles.DropdownContainer}
           
          />
          
<DropDownPicker
            style={styles.ProjectList}
            open={open2}
            value={value2}
            items={[
                {label: 'CURRENT WEEK', value: 'CURRENT WEEK'},
                {label: 'CURRENT MONTH', value: 'CURRENT MONTH'},
                {label: 'TODAY', value: 'TODAY'},
                {label: 'CUSTOM VIEW', value: 'CUSTOM VIEW'},
               
              ]}
            setOpen={setOpen2}
            setValue={setValue2}
            setItems={setItems2}
            placeholder="Project Span"
            listMode = "SCROLLVIEW"
            containerStyle={styles.DropdownContainer}
           
          />
          



      </View>
      
    </View>
    
    </ScrollView>
  )
}

export default BudgetFilters

const styles = StyleSheet.create({
    main:{
        height:height,
        width : width,
        borderWidth:1,
        alignItems:'center',
        justifyContent : 'center',
    },
    Filters :{
        height:height*0.7,
        width :width,
    borderWidth : 1,
    flexDirection : 'row',
    
    },
    ProjectList:{
    borderColor: colours.Green,
    width: width * 0.33,
    height: 50,
    marginVertical:10,
    backgroundColor : colours.White,
    opacity:1,
    borderWidth :1,
    
    },
    DropdownContainer :{
        width : width*0.33,

    },
})