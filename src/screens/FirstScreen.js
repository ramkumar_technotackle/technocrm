import {
  StyleSheet,
  Text,
  View,
  Button,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  Alert,
  Image,
} from 'react-native';
import React from 'react';
import {useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import {colours, LIST_OF_PROJECTS,LIST_OF_LEAVE_REQUESTS} from '../constants';
import axios from 'axios';
import md5 from 'md5';
import {useDispatch} from 'react-redux';
import {setProjects, setUser, setUserID,setLeaveRequests} from '../redux/slice/User';
import {useSelector} from 'react-redux';
import Menu from 'react-native-vector-icons/Entypo';
import Form from 'react-native-vector-icons/MaterialCommunityIcons';
import Logout from 'react-native-vector-icons/Ionicons';
import Dollor from 'react-native-vector-icons/FontAwesome';
import WpForm from 'react-native-vector-icons/FontAwesome';
import SplashScreen from './SplashScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ListOfLeaveRequest from './ListOfLeaveRequest';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;



const FirstScreen = props => {
  const navigation = useNavigation();
  const SALT = 'TTCRM@2020';
  const [loading, setLoading] = useState();
  const user = useSelector(state => state.user.parameters);
  const userID = useSelector(state => state.user.userID);
  console.log('user1', userID);
  const dispatch = useDispatch();
  const [details, setDetails] = useState();




  const showAlert = () => {
    Alert.alert('Alert', 'Do you want to logout', [
      {
        text: 'Yes',
        onPress: () => {
          logout();
        },
        style: 'cancel',
      },
      {
        text: 'No',
        style: 'cancel',
      },
    ]);
  };

  const logout = async () => {
    try {
      await AsyncStorage.removeItem('@store');
      dispatch(setUserID(''));
      navigation.navigate('LoginPage');
    } catch (e) {
      alert('cannot erase stored data');
    }

    console.log('Done.');
  };

  const handleSubmit = () => {
    navigation.navigate('ListOfProjects');
    setLoading(true);
    const auth_token = md5(SALT + userID);
    const receiveEnd = LIST_OF_PROJECTS
    const data1 = {
      developer_id: userID,
      auth_token: auth_token,
    };
    console.log('budget data', data1);
    console.log('login1 is ',receiveEnd);
    axios
      .post(receiveEnd, data1)
      .then(Response => {
        if (Response.data.success) {
          setLoading(false);
          dispatch(setProjects(Response.data.parameters));
          setDetails(Response.data);
        } else {
          setLoading(false);
          alert(Response.data.message);
        }
      })
      .catch(err => {
        console.log(err);
      });
  };


  const handleSubmitRequest = ()=>{
    navigation.navigate('ListOfLeaveRequest')
    setLoading(true);
    const auth_token = md5(SALT + userID);
    const receiveEndRequest = LIST_OF_LEAVE_REQUESTS
    const data1 = {
      developer_id: userID,
      auth_token: auth_token,
    };
    console.log('LeaveRquests', data1);
    
    axios
      .post(receiveEndRequest, data1)
      .then(Response => {
        if (Response.data.success) {
          setLoading(false);
           dispatch(setLeaveRequests(Response.data.parameters));
          console.log('request response',Response.data )
        } else {
          setLoading(false);
          alert(Response.data.message);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  return (
   
        <View>
          <View style={styles.Header}>
            <View style={styles.logo} >
            <Image
                style={styles.logoImage}
                source={require('../assets/Images/TechnoLogo.png')}
              />
            </View>
            <View>
              <Text style={styles.HeaderText}>DASHBOARD</Text>
            </View>

            <View>
              <TouchableOpacity
                onPress={() => {
                  showAlert();
                }}>
                <Logout
                  name="ios-log-out-outline"
                  style={styles.logout}
                  size={30}
                  color={colours.White}
                />

                <Text style={styles.logoutText}>Logout</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.main}>
            <View style={styles.cardBox}>
            
            <View style={styles.FirstRow}>

              <TouchableOpacity
                style={styles.DashBoardBoxes}
                onPress={() => {
                  navigation.navigate('ApplyLeave');
                }}>
                <Form
                  name="form-select"
                  style={styles.DashBoardIcon}
                  size={20}
                />
                <Text style={styles.DashboardText}>APPLY LEAVE</Text>
              </TouchableOpacity>
            </View>

            
            <View style={styles.FirstRow}>
              <TouchableOpacity
                style={styles.DashBoardBoxes}
                onPress={handleSubmitRequest}>
                 <WpForm
                  name="wpforms"
                  style={styles.DashBoardIcon}
                  size={21}
                />
                <Text style={styles.DashboardText}>LEAVE REQUESTS</Text>
              </TouchableOpacity>
            </View>

            <View style={styles.FirstRow}>
              <TouchableOpacity
                style={styles.DashBoardBoxes}
                onPress={() => {
                  handleSubmit();
                }}>
                <Dollor
                  name="dollar"
                  style={{...styles.DashBoardIcon, paddingLeft: 19}}
                  size={20}
                />
                <Text style={styles.DashboardText}>BUDGET TRACKER</Text>
              </TouchableOpacity>
            </View>

            </View>
          </View>
        </View>
    
  );
};

export default FirstScreen;

const styles = StyleSheet.create({
  Header: {
    height: height * 0.07,
    width: width,
    backgroundColor: colours.Green,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 13,
    alignItems: 'center',
  },
  logo:{
    height: height * 0.06,
    width: 60,
    backgroundColor: colours.White,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
  },
  logoImage:{
    height : height*0.055,
    width:50,

  },
  HeaderText: {
    fontSize: 17,
    fontWeight: '600',
    color: colours.White,
  },

  main: {
    height: height,
    width: width,
    alignItems: 'center',
 
    backgroundColor: colours.White,
  },
  cardBox:{
    height: height*0.7,
    width: width,
    alignItems: 'center',
  justifyContent:'center',
    backgroundColor: colours.White,
    alignContent:'stretch',

  },
  DashboardBox: {
    height: height * 0.2,
    width: width * 0.4,
    backgroundColor: colours.Green,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 40,
    textAlign: 'center',
    padding: 20,
  },

  DashboardText: {
    color: colours.Black,
    fontSize: 17,
    textAlign: 'center',
    marginLeft: 70,
  },
  logout: {
    marginLeft: 4,
  },
  logoutText: {
    fontSize: 10,
    color: colours.White,
  },
  FirstRow: {
    flexDirection: 'row',
    width: width,
    justifyContent: 'space-around',
    marginTop: 70,
  },
  DashBoardBoxes: {
    height: height * 0.17,
    width: width * 0.9,
    alignItems: 'center',
    shadowColor: colours.Green,
    shadowOffset: {width: 0, height: 0},
    shadowOpacity: 1,
    elevation: 4,
    borderWidth: 0.1,
    flexDirection: 'row',
  },
  DashBoardIcon: {
    padding: 15,
    height: 50,
    width: 50,
    color: 'white',
    borderRadius: 30,
    marginLeft: 15,
    backgroundColor: colours.Green,
    marginTop: 5,
    alignSelf: 'center',
  },
});
