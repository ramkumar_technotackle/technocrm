import { View, Text, StyleSheet, Dimensions, FlatList, ScrollView,ActivityIndicator, TouchableOpacity } from 'react-native';
import React from 'react';
import Header from '../components/Header'
import { colours } from '../constants'; 
import { useSelector } from 'react-redux';
import {useNavigation}  from '@react-navigation/native';



const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width

const ListOfLeaveRequest = () => {
    const navigation = useNavigation();
    const leaveRequests = useSelector(state => state.user.leaveRequests)
    console.log('leavereq', leaveRequests)

    return (
        <ScrollView>
            <Header name="Leave Requests" />
           { leaveRequests ? <View style={styles.main} >
                <FlatList
                    nestedScrollEnabled={true}
                    data={leaveRequests}
                    onEndReachedThreshold={0}
                    renderItem={({ item }) => (<TouchableOpacity style={styles.dataCard}
                    onPress={()=>{navigation.navigate("ViewLeaveRequest",{leaveDetail:item})}}>
                        <View style={styles.dataBoxHeader}>
                            <Text style={{ ...styles.dataCardText, fontWeight: '500' }}>
                                {item.leave_type}
                            </Text>
                        </View>

                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Name
                            </Text>
                            <Text style={styles.dataTextRight}>
                                {item.developer.developer_name}
                            </Text>
                        </View>
                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Date Of Leave
                            </Text>
                            <Text style={styles.dataTextRight}>
                                {item.date_of_leave}
                            </Text>
                        </View>
                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Comments
                            </Text>
                            <Text style={styles.dataTextRight}>
                                {item.dev_comments}
                            </Text>
                        </View>
                        <View style={styles.dataBox}>
                            <Text style={styles.dataCardText}>
                                Status
                            </Text>
                            <Text style={styles.dataTextRight}>
                                {item.status}
                            </Text>
                        </View>
                    </TouchableOpacity>)

                    }
                />

            </View> : <View>
            <ActivityIndicator size="large" color="#00ff00" />
          </View>}
        </ScrollView>
    )
}
export default ListOfLeaveRequest;

const styles = StyleSheet.create({
    main: {
        height: height * 1,
        width: width * 1,

    },

    dataCard: {
        height: height * 0.27,
        width: width * 0.9,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        elevation: 4,
        marginVertical:10,
        backgroundColor: '#ffffff',
        alignSelf: 'center',
        paddingRight: 3,
        paddingLeft: 3,  
    },
    dataBox: {
        marginVertical: 5,
        padding: 5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        borderBottomColor: colours.Green,
        backgroundColor: colours.White,
    },
    dataCardText: {
        color: colours.Black,
        fontSize: 15,
    },
    dataTextRight: {
        fontSize: 15,
    },
    dataBoxHeader:{
        marginVertical: 5,
        padding: 5,
        justifyContent: 'center', 
        alignItems:'center',
        borderBottomWidth: 1,
         backgroundColor: '#f2e58c', 
        borderWidth: 1,
         borderColor: colours.Green,
    }

})