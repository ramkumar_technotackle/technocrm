import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {colours} from '../constants';

const height = Dimensions.get('window').height;
const width = Dimensions.get('window').width;

const Header = props => {
  const navigation = useNavigation();

  return (

      <View style={styles.Header}>
        <TouchableOpacity style={styles.iconBox}
          onPress={() => {
            navigation.goBack();
          }}>
          <Icon
            name="arrowleft"
            style={styles.Arrow}
            size={40}
            color="#000000"
          />
        </TouchableOpacity>
        <View style={styles.headerContent}>
        <Text style={styles.HeaderText}>{props.name}</Text>
        </View>
        <View style ={styles.border}></View>
      </View>
    
  );
};

export default Header;

const styles = StyleSheet.create({
  Header: {
    height: height * 0.07,
    width: width,
    backgroundColor: colours.Green,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
  },
  HeaderText: {
    fontSize: 17,
    fontWeight: '600',
    color: colours.White,
    textAlign:'center',

  },
  headerContent:{
    alignSelf:'center',
    width:width*0.6,
    
    
  },
  Arrow: {
    padding: 10,
    color: colours.White,
  },
  iconBox:{
    height:height*0.07,
    width: width*0.2,
 
     },
     border:{
      width: width*0.2,
     
     }
});
